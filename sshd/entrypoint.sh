#!/bin/bash

# Container configuration environment variables
#
# ROOT_PASSWORD      : The root password to set
# USER               : An optional user to create
# USER_PASSWORD      : The password for the optional user
# SSH_AUTHORIZED_KEY : An authorized ssh public key to authenticate to the root or ssh user account

DONE_FILE=/setupuser.done

if [ ! -f ${DONE_FILE} ]; then

    # Setup password and authorized key for root
    if [ -n "${ROOT_PASSWORD}" ]; then
        # ROOT_CRYPT_PASSWORD=$(echo "${ROOT_PASSWORD}" | mkpasswd -m sha-512 -s)
        # usermod -p ${ROOT_CRYPT_PASSWORD} root
        echo "root:${ROOT_PASSWORD}" | chpasswd
    fi

    if [ -n "${SSH_AUTHORIZED_KEY}" ]; then
        mkdir /root/.ssh
        echo "${SSH_AUTHORIZED_KEY}" > /root/.ssh/authorized_keys
    fi

    # Set a user with password and authoritzed key
    if [ -n "${USER}" ]; then

        if [ ! -d "/home/${USER}" ]; then

            if [ -z "${USER_PASSWORD}" ]; then
                USER_PASSWORD=$(pwgen -c -n -1 12)
                echo "New user password is '${USER_PASSWORD}'"
            fi
            # USER_CRYPT_PASSWORD=$(echo "${USER_PASSWORD}" | mkpasswd -m sha-512 -s)
            # groupadd -g 1000 ${USER}
            # useradd -g 1000 -u 1000 -d /home/${USER} -m -k /etc/skel -s /bin/bash -p ${USER_CRYPT_PASSWORD} ${USER}
            groupadd -g 1000 ${USER}
            useradd -g 1000 -u 1000 -d /home/${USER} -m -k /etc/skel -s /bin/bash ${USER}
            echo "${USER}:${USER_PASSWORD}" | chpasswd

            if [ -n "${SSH_AUTHORIZED_KEY}" ]; then
                mkdir /home/${USER}/.ssh
                echo "${SSH_AUTHORIZED_KEY}" > /home/${USER}/.ssh/authorized_keys
            fi
        fi
    fi

    echo "done" > ${DONE_FILE}
fi

/usr/sbin/sshd -D
