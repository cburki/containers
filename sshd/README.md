# Summary

A container with a ssh server.

The builder assume you are building the image on a Linux distibution that uses `dnf` as its package manager.


# Build the image

    ./build.sh <imagename>


# Configure the container

The following environment variables could be set to configure the images.

- ROOT_PASSWORD : The password for the root user.
- USER : An optional user that will be created.
- USER_PASSWORD : The password of the optional user.
- SSH_AUTHORIZED_KEY : A public key that can be used for the authentication of the root or user account.


# Run the container

    podman run --name <name> -e ROOT_PASSWORD=secret -e USER=myuser -e USER_PASSWORD=secret -p 2222:22 -d <imagename>
