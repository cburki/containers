#!/bin/bash

# Image build arguments
# $1 : The image name including tag

imagename=$1
if [ -z "$imagename" ]; then
    echo "build.sh <imagename>"
    echo "    The image name has to be given"
    exit 1
fi


container=$(buildah from --name working-sshd scratch)
mountpoint=$(buildah mount ${container})

# Install the openssh server
DNF="dnf install -y --releasever 30 --installroot ${mountpoint} --setopt=install_weak_deps=false"
${DNF} openssh-server

buildah run ${container} -- ssh-keygen -A
sed -ri 's/UsePAM yes/UsePAM no/g' ${mountpoint}/etc/ssh/sshd_config

cp entrypoint.sh ${mountpoint}/entrypoint.sh
chmod a+x ${mountpoint}/entrypoint.sh

# Configure the container image
buildah config --label maintainer="Christophe Burki <christophe.burki@protonmail.ch>" ${container}
buildah config --entrypoint "/entrypoint.sh" ${container}

# Commit the container image
buildah commit ${container} ${imagename}

exit 0
