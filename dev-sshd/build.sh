#!/bin/bash

# Container build arguments
# $1 : The image name including tag

# Container configuration environment variables
#
# ROOT_PASSWORD      : The root password to set
# USER               : An optional user to create
# USER_PASSWORD      : The password for the optional user
# SSH_AUTHORIZED_KEY : An authorized ssh public key to authenticate to the root or ssh user account

imagename=$1
if [ -z "$imagename" ]; then
    echo "build.sh <imagename>"
    echo "    The image name has to be given"
    exit 1
fi


container=$(buildah from --name working-dev-sshd registry.gitlab.com/cburki/containers/sshd:latest)
mountpoint=$(buildah mount ${container})

# Install the openssh server
DNF="dnf install -y --releasever 30 --installroot ${mountpoint} --setopt=install_weak_deps=false"
${DNF} python3

# Configure the container image
buildah config --label maintainer="Christophe Burki <christophe.burki@protonmail.ch>" ${container}

# Commit the container image
buildah commit ${container} ${imagename}

exit 0
